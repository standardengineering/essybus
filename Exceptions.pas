﻿namespace Essy.EssyBus;

interface

uses
  System.Collections.Generic,
  System.Text;

type
  EssyBusException = public class(Exception);

  PhysicalLayerException = public class(EssyBusException);

  EssyBusClientException = public class(EssyBusException);

  FatalEssyBusException = public class(EssyBusException); 
  
implementation

end.