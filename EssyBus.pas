﻿namespace Essy.EssyBus;

interface

uses
  System.Threading,
  System.Collections.Generic;

type
  /// <summary>This class handles the data transfer over the EssyBus.</summary>
  EssyBus = public class
  private
    fFatalErrorOccurred: Boolean;
    fAllClients: List<EssyBusClient> := new List<EssyBusClient>;
    fClientList: List<IEssyBusClientInterrupt> := new List<IEssyBusClientInterrupt>;
    fResetableClientList: List<IEssyBusClientResetable> := new List<IEssyBusClientResetable>;
    fPhysicalLayer: IPhysicalLayer;
    fLastSendAddress: Byte := 0;
    rnd: Random := new Random;
    method FromHexArray(aInput: Array of Byte; aInputStartOffSet: Int32): Byte;
    method ToHexArray(aInput, aOutput: Array of Byte; aOutputOffSet: Int32);
    method FromHexArray(aInput: Array of Byte; aInputStartOffSet, aInputEndOffSet: Int32): Array of Byte;
    method fRandomSleep(min, max: Int32);
    method set_PhysicalLayer(value: IPhysicalLayer);
    method fInterruptLineChanged(sender: IPhysicalLayer);
    method fReadWriteDataToDevice(aDevice: EssyBusClient; aDataToWrite: array of Byte; aResponseExpected: Boolean; var aDataToRead: array of Byte; aClearBuffers: Boolean): Exception;
    const
      ACK: Byte = 252;
      STX: Byte = 255;
      EOT: Byte = 253;
      ETX: Byte = 254;
  protected
  assembly
    method ReadWriteDataToDevice(aDevice: EssyBusClient; aData: array of Byte; aResponseExpected: Boolean): array of Byte; locked;
  public
    /// <summary>Gets/Sets the physical layer to communicate with the EssyBus.</summary>
    /// <value>the physical layer</value>
    /// <returns>the physical layer</returns>
    property PhysicalLayer: IPhysicalLayer read fPhysicalLayer write set_PhysicalLayer;
    property AutoResetDevices: Boolean := true;
    property DisableRetries: Boolean := false;
    property ClientList: List<EssyBusClient> read fAllClients;
    method AddClient(aClient: EssyBusClient);
    method ResetDevices;
    method WriteHexFileToDevice(aClient: EssyBusClient; aHexFile: String): Boolean;
    method GetVersionInfoFromFollowers(): sequence of FollowerInfo; iterator;
    constructor(aPhysicalLayer: IPhysicalLayer);
  end;

  /// <summary>This delegate is used when the Physical layer needs to let the EssyBys class know that the interrupt line changed status.</summary>
  PhysicalLayerInterruptEventHandler = public delegate(sender: IPhysicalLayer);

  IPhysicalLayer = public interface
    method ReadFromEEProm(aSize: UInt32): array of Byte;
    method WriteToEEProm(aData: array of Byte);
    method WriteBytes(aData: array of Byte);
    method ReadBytes(aNumberOfBytes: Byte): array of Byte;  
    method GetInterruptLineStatus: Boolean;
    method ResetInterface: Boolean;
    method ClearBuffers;
    event InterruptLineChange: PhysicalLayerInterruptEventHandler;
    event ThreadException: ThreadExceptionEventHandler;
  end;

  FollowerInfo = public class
  private
  public
    property Name: String;
    property Version: Version;
    property Address: Byte;
    method ToString: String; override;
  end;

implementation

method EssyBus.ToHexArray(aInput, aOutput: Array of Byte; aOutputOffSet: Int32);
begin
  var inputArrayLength := aInput.Length;
  var indx := aOutputOffSet;
  for each i in aInput do
  begin
    aOutput[indx] := i mod 16;
    aOutput[indx + 1] := i div 16;
    inc(indx, 2);  
  end;
end;

method EssyBus.FromHexArray(aInput: Array of Byte; aInputStartOffSet, aInputEndOffSet: Int32): Array of Byte;
begin
  result := new Byte[((aInputEndOffSet - aInputStartOffSet) + 1) div 2];
  var indx := 0;
  for i: Int32 := aInputStartOffSet to aInputEndOffSet step 2 do
  begin
    result[indx] := aInput[i] + (aInput[i+1] * 16);
    inc(indx);  
  end;
end;

method EssyBus.FromHexArray(aInput: Array of Byte; aInputStartOffSet: Int32): Byte;
begin
  result := aInput[aInputStartOffSet] + (aInput[aInputStartOffSet+1] * 16);
end;

/// <summary>Creates a EssyBus object.</summary>
/// <param name="aPhysicalLayer">Any Object that implements the IPhysicalLayer interface</param>
constructor EssyBus(aPhysicalLayer: IPhysicalLayer);
begin
  self.PhysicalLayer := aPhysicalLayer;
end;

method EssyBus.fInterruptLineChanged(sender: IPhysicalLayer);
begin
  for each client in self.fClientList do
  begin
    if client.WaitingForInterrupt then
    begin   
      client.HandleInterrupt();
      if not sender.GetInterruptLineStatus then exit;
    end;
  end;
end;

/// <summary>Sets the physical layer to communicate with the EssyBus.</summary>
/// <param name="value">the physical layer</param>
method EssyBus.set_PhysicalLayer(value: IPhysicalLayer);
begin
  if self.fPhysicalLayer <> nil then  //remove previous delegate
  begin
    self.fPhysicalLayer.InterruptLineChange -= @self.fInterruptLineChanged;
  end;
  if value <> nil then
  begin
    self.fPhysicalLayer := value;
    self.fPhysicalLayer.InterruptLineChange += @self.fInterruptLineChanged;
    self.PhysicalLayer.ClearBuffers();
  end;
end;

method EssyBus.ReadWriteDataToDevice(aDevice: EssyBusClient; aData: Array of Byte; aResponseExpected: Boolean): Array of Byte;
begin
  if fFatalErrorOccurred then exit nil;
  var retryCounter := 0;
  var tryResult := self.fReadWriteDataToDevice(aDevice, aData, aResponseExpected, var result, false);
  if (tryResult <> nil) and self.DisableRetries then raise new EssyBusException(String.Format(Essy.EssyBus.Properties.Resources.strCould_not_readwrite_DATA_tofrom_device_0__1, aDevice.Name, aDevice.Address), tryResult);
  while tryResult <> nil do
  begin
    if fFatalErrorOccurred then exit nil;
    inc(retryCounter);
    if retryCounter >= 20 then
    begin
      raise new EssyBusException(String.Format(Essy.EssyBus.Properties.Resources.strCould_not_readwrite_DATA_tofrom_device_0__1, aDevice.Name, aDevice.Address), tryResult);
    end;
    if retryCounter = 10 then
    begin
      if not self.fPhysicalLayer.ResetInterface() then 
      begin
        fFatalErrorOccurred := true;
        raise new FatalEssyBusException(Essy.EssyBus.Properties.Resources.strThe_system_stopped_communicating_with_the_device_);
      end;
    end;
    if fFatalErrorOccurred then exit nil;
    fRandomSleep(200, 500); //wait 200 - 500 mS
    tryResult := self.fReadWriteDataToDevice(aDevice, aData, aResponseExpected, var result, true);
  end;
  if retryCounter <> 0 then System.Diagnostics.Debug.WriteLine(String.Format(Essy.EssyBus.Properties.Resources.str0___of_retries__1_while_comunicating_with__2, DateTime.Now, retryCounter, aDevice.Name));
end;

/// <summary>Adds a EssyBusClient object to the EssyBus.</summary>
/// <param name="aClient">a EssyBusClient object.</param>
method EssyBus.AddClient(aClient: EssyBusClient);
begin
  self.fAllClients.Add(aClient);
  if aClient is IEssyBusClientInterrupt then self.fClientList.Add(IEssyBusClientInterrupt(aClient));
  aClient.SetEssyBus(self);
  if aClient is IEssyBusClientResetable then 
  begin
    var client := IEssyBusClientResetable(aClient);
    if self.AutoResetDevices then client.ResetDevice;
    self.fResetableClientList.Add(client);
  end;
end;

method EssyBus.fReadWriteDataToDevice(aDevice: EssyBusClient; aDataToWrite: array of Byte; aResponseExpected: Boolean; var aDataToRead: array of Byte; aClearBuffers: Boolean): Exception;
begin
 try
    if aClearBuffers then self.fPhysicalLayer.ClearBuffers();
    var sendBuffer := new Byte[(aDataToWrite.Length * 2) + 6]; //STX + Dest Address + Source Address + Data length + checksum + ETX = 6
    sendBuffer[0] := STX;
    var sendBufferLength := aDataToWrite.Length;
    if sendBufferLength > 250 then exit(new EssyBusException(Essy.EssyBus.Properties.Resources.strData_larger_than_250_Bytes + Environment.NewLine + Essy.EssyBus.Properties.Resources.strCheck_to_ensure_cabling_is_properly_connected_and_secured_));
    sendBuffer[1] := aDevice.Address;
    sendBuffer[2] := 0; //leader address is 0;
    sendBuffer[3] := sendBufferLength;
    ToHexArray(aDataToWrite, sendBuffer, 4);
    var sum: UInt16 := 0;
    for each i in aDataToWrite do
    begin
      sum := sum + i;
    end;
    var checksum: Byte := sum mod 250;
    sendBuffer[(aDataToWrite.Length * 2) + 4] := checksum;
    sendBuffer[(aDataToWrite.Length * 2) + 5] := ETX;
    self.fPhysicalLayer.WriteBytes(sendBuffer);
    var hexResponse := self.fPhysicalLayer.ReadBytes(2);
    if hexResponse[0] <> aDevice.Address then exit(new EssyBusException(String.Format(Essy.EssyBus.Properties.Resources.strNot_the_correct_address_received_Expected1_Received0, hexResponse[0], aDevice.Address) + Environment.NewLine + Essy.EssyBus.Properties.Resources.strCheck_to_ensure_cabling_is_properly_connected_and_secured_));
    if hexResponse[1] <> ACK then exit(new EssyBusException(Essy.EssyBus.Properties.Resources.strNo_ACK_received + Environment.NewLine + Essy.EssyBus.Properties.Resources.strCheck_to_ensure_cabling_is_properly_connected_and_secured_));
    if aResponseExpected then
    begin
      hexResponse := self.fPhysicalLayer.ReadBytes(4);
      if hexResponse[0] <> STX then exit(new EssyBusException(Essy.EssyBus.Properties.Resources.strNo_STX_received + Environment.NewLine + Essy.EssyBus.Properties.Resources.strCheck_to_ensure_cabling_is_properly_connected_and_secured_));
      if hexResponse[1] <> 0 then exit(new EssyBusException('wrong leader address' + Environment.NewLine + Essy.EssyBus.Properties.Resources.strCheck_to_ensure_cabling_is_properly_connected_and_secured_));
      if hexResponse[2] <> aDevice.Address then exit(new EssyBusException('Not the correct follower address received' + Environment.NewLine + Essy.EssyBus.Properties.Resources.strCheck_to_ensure_cabling_is_properly_connected_and_secured_));
      var receiveBufferLength := hexResponse[3];
      var receiveHexBufferLength := (receiveBufferLength * 2) + 2;
      // Check if receiveHexBufferLength is not higher than 255, if so ReadBytes will cause overflow (parameter is of type Byte) 
      // and getting the receivedChecksum will cause and index out of range exception (see case 8524) 
      // exit with EssybusException so this is regarded as a hardware issue
      if (receiveHexBufferLength > 255) then exit (new EssyBusException('Received buffer length in hex response caused overflow')); 
      var receiveHexBuffer := self.fPhysicalLayer.ReadBytes(receiveHexBufferLength);
      var receivedChecksum := receiveHexBuffer[receiveHexBufferLength - 2];
      aDataToRead := FromHexArray(receiveHexBuffer, 0, receiveHexBufferLength - 3); 
      sum := 0;
      for each i in aDataToRead do
      begin
        sum := sum + i;
      end;
      checksum := sum mod 250;
      if checksum <> receivedChecksum then exit(new EssyBusException(Essy.EssyBus.Properties.Resources.strWrong_Checksum_received + Environment.NewLine + Essy.EssyBus.Properties.Resources.strCheck_to_ensure_cabling_is_properly_connected_and_secured_));
      if receiveHexBuffer[receiveHexBufferLength - 1] <> ETX then exit(new EssyBusException(Essy.EssyBus.Properties.Resources.strNo_ETX_received + Environment.NewLine + Essy.EssyBus.Properties.Resources.strCheck_to_ensure_cabling_is_properly_connected_and_secured_));
    end;
    var response := self.fPhysicalLayer.ReadBytes(1);
    if response[0] <> EOT then 
    begin
      exit(new EssyBusException(Essy.EssyBus.Properties.Resources.strEOT_expected + Environment.NewLine + Essy.EssyBus.Properties.Resources.strCheck_to_ensure_cabling_is_properly_connected_and_secured_));
    end;
    exit(nil); //transmission OK
  except
    on ex: PhysicalLayerException do
    begin
      exit(ex);
    end;
  end;
end;

method EssyBus.fRandomSleep(min, max: Int32);
begin
  var randInt := Int32(Math.Round(self.rnd.NextDouble * (max - min)));
  Thread.Sleep(min + randInt);
end;

method EssyBus.ResetDevices;
begin
  for each dev in self.fResetableClientList do dev.ResetDevice();
end;

method EssyBus.WriteHexFileToDevice(aClient: EssyBusClient; aHexFile: String): Boolean;
begin
  for each c in self.fAllClients do
  begin
    if c <> aClient then
    begin
      c.DisconnectedFromBus();
    end;
  end;
  aClient.RebootDevice();
  Thread.Sleep(1000); //wait for reboot
  
end;

method EssyBus.GetVersionInfoFromFollowers(): sequence of FollowerInfo;
begin
  for each follower in self.fAllClients do
  begin
    yield new FollowerInfo(Name := follower.Name, Address := follower.Address, Version := follower.FirmwareVersion);
  end;
end;

method FollowerInfo.ToString: String;
begin
  result := String.Format('{0} Address: {1} Version: {2}', self.Name, self.Address, self.Version);
end;

end.