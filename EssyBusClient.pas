﻿namespace Essy.EssyBus;

interface

uses
  System.Collections.Generic,
  System.Collections,
  System.Text;

type
  EssyBusClient = public abstract class
  private
    fEssyBus: EssyBus;
    fVersion: Version;
    method get_FirmwareVersion: Version;
  protected
    method EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Array of Byte);
    method EncodeFunction(aMethodTokenNumber: Byte; aParam1: Byte; out aReturnedData: Array of Byte);
    method EncodeFunction(aMethodTokenNumber: Byte; aParam1: Byte; out aReturnedData: Byte);
    method EncodeFunction(aMethodTokenNumber: Byte; aParam1: UInt16; out aReturnedData: Int32);
    method EncodeFunction(aMethodTokenNumber: Byte; aParam1: Byte; out aReturnedData: Double);
    method EncodeFunction(aMethodTokenNumber: Byte; aParam1: Byte; aParam2: Byte; out aReturnedData: Double);
    method EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Int32);
    method EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: UInt32);
    method EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: UInt16);
    method EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Int16);
    method EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Byte);
    method EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Boolean);
    method EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: String);
    method EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Single);
    method EncodeFunction(aMethodTokenNumber: Byte; aParam1: Byte; out aReturnedData: Single);
    method EncodeMethod(aMethodTokenNumber: Byte; aParam1: Int32);
    method EncodeMethod(aMethodTokenNumber: Byte; aParam1: Single);
    method EncodeMethod(aMethodTokenNumber: Byte; aParam1: Byte; aParam2: Single);
    method EncodeMethod(aMethodTokenNumber: Byte; aParam1: UInt16);
    method EncodeMethod(aMethodTokenNumber: Byte; aParam1, aParam2: UInt16);
    method EncodeMethod(aMethodTokenNumber: Byte; aParam1: Boolean);
    method EncodeMethod(aMethodTokenNumber: Byte; aParam1, aParam2: Byte; aParam3: Double);
    method EncodeMethod(aMethodTokenNumber: Byte; aParam1, aParam2: Boolean);
    method EncodeMethod(aMethodTokenNumber: Byte; aParam1: Int32; aParam2, aParam3: Boolean);
    method EncodeMethod(aMethodTokenNumber, aParam1: Byte; aParam2: array of Byte);
    method EncodeMethod(aMethodTokenNumber, aParam1: Byte; aParam2: Double);
    method EncodeMethod(aMethodTokenNumber: Byte; aParam1: Array of Byte);
    method EncodeMethod(aMethodTokenNumber, aParam1: Byte);
    method EncodeMethod(aMethodTokenNumber, aParam1, aParam2: Byte);
    method EncodeMethod(aMethodTokenNumber, aParam1, aParam2, aParam3: Byte);
    method EncodeMethod(aMethodTokenNumber: Byte);
    method BoolToByte(aBool: Boolean): Byte;
  assembly or protected
    method SetEssyBus(aEssyBus: EssyBus);
  public
    property Address: Byte;
    property Name: String;
    property FirmwareVersion: Version read get_FirmwareVersion;
    property FirmwareVersionContainsPCBRev: Boolean;
    method Ping(aByte: Byte): Byte;
    method RebootDevice;
    method DisconnectedFromBus;
    method ToString: String; override;
    constructor(aAddress: Byte; aName: String);
  end;

  IEssyBusClientResetable = public interface
    method ResetDevice();
  end;

  IEssyBusClientInterrupt = public interface(IEssyBusClientResetable)
    method HandleInterrupt();
    property WaitingForInterrupt: Boolean read;
  end;

implementation

method EssyBusClient.SetEssyBus(aEssyBus: EssyBus);
begin
  self.fEssyBus := aEssyBus;
end;

constructor EssyBusClient(aAddress: Byte; aName: String);
begin
  self.Address := aAddress;
  self.Name := aName;
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Array of Byte);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber];
  aReturnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, True);  
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber];
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, False);  
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Int32);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, True);  
  if assigned(returnedData) then aReturnedData := BitConverter.ToInt32(returnedData, 0);
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte; aParam1: Int32);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[5]; 
  sendBuffer[0] := aMethodTokenNumber;
  BitConverter.GetBytes(aParam1).CopyTo(sendBuffer, 1);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, False);  
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Byte);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, True);  
  if assigned(returnedData) then aReturnedData := returnedData[0];
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber, aParam1: Byte);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[2]; 
  sendBuffer[0] := aMethodTokenNumber;
  sendBuffer[1] := aParam1;
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: String);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  var enc := new System.Text.ASCIIEncoding;
  if assigned(returnedData) then aReturnedData := enc.GetString(returnedData, 0, returnedData.Length);
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: UInt16);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  if assigned(returnedData) then aReturnedData := BitConverter.ToUInt16(returnedData, 0);
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte; aParam1: UInt16);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[3]; 
  sendBuffer[0] := aMethodTokenNumber;
  BitConverter.GetBytes(aParam1).CopyTo(sendBuffer, 1);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte; aParam1: Boolean);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[2]; 
  sendBuffer[0] := aMethodTokenNumber;
  sendBuffer[1] := BoolToByte(aParam1);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber, aParam1: Byte; aParam2: array of Byte);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[aParam2.Length + 2]; 
  sendBuffer[0] := aMethodTokenNumber;
  sendBuffer[1] := aParam1;
  aParam2.CopyTo(sendBuffer, 2);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; aParam1: Byte; out aReturnedData: array of Byte);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber, aParam1];
  aReturnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte; aParam1, aParam2: Boolean);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[3]; 
  sendBuffer[0] := aMethodTokenNumber;
  sendBuffer[1] := BoolToByte(aParam1);
  sendBuffer[2] := BoolToByte(aParam2);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.BoolToByte(aBool: Boolean): Byte;
begin
  result := iif(aBool, 255, 0);
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Boolean);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  if assigned(returnedData) then aReturnedData := Boolean(returnedData[0]);
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: UInt32);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  if assigned(returnedData) then aReturnedData := BitConverter.ToUInt32(returnedData, 0);
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber, aParam1, aParam2: Byte);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[3]; 
  sendBuffer[0] := aMethodTokenNumber;
  sendBuffer[1] := aParam1;
  sendBuffer[2] := aParam2;
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber, aParam1, aParam2, aParam3: Byte);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[4]; 
  sendBuffer[0] := aMethodTokenNumber;
  sendBuffer[1] := aParam1;
  sendBuffer[2] := aParam2;
  sendBuffer[3] := aParam3;
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Int16);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  if assigned(returnedData) then aReturnedData := BitConverter.ToInt16(returnedData, 0);
end;

method EssyBusClient.Ping(aByte: Byte): Byte;
begin
  self.EncodeFunction(0, aByte, out result);
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; aParam1: Byte; out aReturnedData: Byte);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber, aParam1];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  if assigned(returnedData) then aReturnedData := returnedData[0];
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; out aReturnedData: Single);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  if assigned(returnedData) then aReturnedData := BitConverter.ToSingle(returnedData, 0);
end;

method EssyBusClient.get_FirmwareVersion: Version;
begin
  if not assigned(fVersion) then
  begin
    var tempVersion := new Byte[4]; // empty byte array
    self.EncodeFunction(254, out tempVersion);
    fVersion := new Version(tempVersion[0], tempVersion[1], tempVersion[2], tempVersion[3]);
  end;
  result := fVersion;
end;

method EssyBusClient.ToString: String;
begin
  result := self.Name + ' @ ' + self.Address.ToString;
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; aParam1: Byte; aParam2: Byte; out aReturnedData: Double);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber, aParam1, aParam2];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  if assigned(returnedData) then aReturnedData := BitConverter.ToDouble(returnedData, 0);
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte; aParam1, aParam2: Byte; aParam3: Double);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[11];
  sendBuffer[0] := aMethodTokenNumber;
  sendBuffer[1] := aParam1;
  sendBuffer[2] := aParam2;
  BitConverter.GetBytes(aParam3).CopyTo(sendBuffer, 3);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte; aParam1: array of Byte);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[aParam1.Length + 1]; 
  sendBuffer[0] := aMethodTokenNumber;
  aParam1.CopyTo(sendBuffer, 1);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte; aParam1: Single);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[5]; 
  sendBuffer[0] := aMethodTokenNumber;
  BitConverter.GetBytes(aParam1).CopyTo(sendBuffer, 1);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.RebootDevice;
begin
  self.EncodeMethod(253);
end;

method EssyBusClient.DisconnectedFromBus;
begin
  self.EncodeMethod(252);
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte; aParam1, aParam2: UInt16);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[5]; 
  sendBuffer[0] := aMethodTokenNumber;
  BitConverter.GetBytes(aParam1).CopyTo(sendBuffer, 1);
  BitConverter.GetBytes(aParam2).CopyTo(sendBuffer, 3);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; aParam1: Byte; out aReturnedData: Double);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber, aParam1];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  if assigned(returnedData) then aReturnedData := BitConverter.ToDouble(returnedData, 0);
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber, aParam1: Byte; aParam2: Double);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[10]; 
  sendBuffer[0] := aMethodTokenNumber;
  sendBuffer[1] := aParam1;
  BitConverter.GetBytes(aParam2).CopyTo(sendBuffer, 2);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; aParam1: UInt16; out aReturnedData: Int32);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[3];
  sendBuffer[0] := aMethodTokenNumber;
  BitConverter.GetBytes(aParam1).CopyTo(sendBuffer, 1);
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  if assigned(returnedData) then aReturnedData := BitConverter.ToInt32(returnedData, 0);
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte; aParam1: Int32; aParam2: Boolean; aParam3: Boolean);
require
  self.fEssyBus <> nil;
begin
  var sendBuffer := new Byte[7];
  sendBuffer[0] := aMethodTokenNumber;
  sendBuffer[5] := BoolToByte(aParam2);
  sendBuffer[6] := BoolToByte(aParam3);
  BitConverter.GetBytes(aParam1).CopyTo(sendBuffer, 1);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

method EssyBusClient.EncodeFunction(aMethodTokenNumber: Byte; aParam1: Byte; out aReturnedData: Single);
begin
  var sendBuffer: array of Byte := [aMethodTokenNumber, aParam1];
  var returnedData := self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, true);  
  if assigned(returnedData) then aReturnedData := BitConverter.ToSingle(returnedData, 0);
end;

method EssyBusClient.EncodeMethod(aMethodTokenNumber: Byte; aParam1: Byte; aParam2: Single);
begin
  var sendBuffer := new Byte[6]; 
  sendBuffer[0] := aMethodTokenNumber;
  sendBuffer[1] := aParam1;
  BitConverter.GetBytes(aParam2).CopyTo(sendBuffer, 2);
  self.fEssyBus.ReadWriteDataToDevice(self, sendBuffer, false);  
end;

end.