﻿<?xml version="1.0" encoding="utf-8" standalone="yes"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
  <PropertyGroup>
    <ProductVersion>3.5</ProductVersion>
    <RootNamespace>Essy.EssyBus</RootNamespace>
    <OutputType>Library</OutputType>
    <BinaryName>EssyBus</BinaryName>
    <AllowGlobals>False</AllowGlobals>
    <AllowLegacyOutParams>False</AllowLegacyOutParams>
    <AllowLegacyCreate>False</AllowLegacyCreate>
    <Configuration Condition="'$(Configuration)' == ''">Debug</Configuration>
    <Platform Condition="'$(Platform)' == ''">AnyCPU</Platform>
    <ProjectGuid>{F45CE426-5D40-4378-8024-CC2A23B9D619}</ProjectGuid>
    <RunPostBuildEvent>OnBuildSuccess</RunPostBuildEvent>
    <ProjectView>ShowAllFiles</ProjectView>
    <Mode>Echoes</Mode>
    <TargetFramework>.NETFramework4.8</TargetFramework>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)' == 'Debug'">
    <ConditionalDefines>DEBUG;TRACE;</ConditionalDefines>
    <OutputPath>.\bin\Debug\</OutputPath>
    <GeneratePDB>True</GeneratePDB>
    <WarnOnCaseMismatch>True</WarnOnCaseMismatch>
    <RunCodeAnalysis>True</RunCodeAnalysis>
    <RequireExplicitLocalInitialization>True</RequireExplicitLocalInitialization>
    <CaptureConsoleOutput>True</CaptureConsoleOutput>
    <EnableUnmanagedDebugging>False</EnableUnmanagedDebugging>
    <CpuType>anycpu</CpuType>
    <XmlDocWarningLevel>WarningOnPublicMembers</XmlDocWarningLevel>
    <Optimize>False</Optimize>
  </PropertyGroup>
  <PropertyGroup Condition="'$(Configuration)' == 'Release'">
    <OutputPath>.\bin\Release</OutputPath>
    <EnableAsserts>False</EnableAsserts>
    <CpuType>anycpu</CpuType>
  </PropertyGroup>
  <ItemGroup>
    <Reference Include="mscorlib" />
    <Reference Include="System" />
    <Reference Include="System.Core" />
  </ItemGroup>
  <ItemGroup>
    <Compile Include="EssyBus.pas" />
    <Compile Include="EssyBusClient.pas" />
    <Compile Include="Exceptions.pas" />
    <Compile Include="Properties\AssemblyInfo.pas" />
    <EmbeddedResource Include="Properties\Resources.cs.resx" />
    <EmbeddedResource Include="Properties\Resources.de.resx" />
    <EmbeddedResource Include="Properties\Resources.es.resx" />
    <EmbeddedResource Include="Properties\Resources.fr.resx" />
    <EmbeddedResource Include="Properties\Resources.it.resx" />
    <EmbeddedResource Include="Properties\Resources.pt-BR.resx" />
    <EmbeddedResource Include="Properties\Resources.resx">
      <Generator>PublicResXFileCodeGenerator</Generator>
    </EmbeddedResource>
    <Compile Include="Properties\Resources.Designer.pas" />
    <None Include="Properties\Settings.settings">
      <Generator>SettingsSingleFileGenerator</Generator>
    </None>
    <Compile Include="Properties\Settings.Designer.pas" />
  </ItemGroup>
  <ItemGroup>
    <Folder Include="Properties\" />
  </ItemGroup>
  <Import Project="$(MSBuildExtensionsPath)\RemObjects Software\Elements\RemObjects.Elements.Echoes.targets" />
</Project>